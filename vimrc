" Vim global configuration.
" Fun stuff.
if has('vim_starting')	
	set nocompatible
	set runtimepath +=~/.vim/bundle/neobundle.vim/
endif

" Demand the world play in Unicode UTF8

set encoding=utf-8

" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Things I've picked up

au BufNewFile,BufRead *.py set
    \ tabstop=4
    \ softtabstop=4
    \ shiftwidth=4
    \ expandtab
    \ autoindent
    \ fileformat=unix

au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

let mapleader = ";"

" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NeoBundle configuration

filetype plugin indent on
call neobundle#begin(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shugo/neobundle.vim'
" NeoBundle 'chriskempson/base16-vim.git'
NeoBundle 'NLKNguyen/papercolor-theme'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'bling/vim-airline'
NeoBundle 'nathanaelkane/vim-indent-guides'
NeoBundleCheck

call neobundle#end()

" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Common configuration

" Always show the statusbar
set laststatus=2
" Always get line numbers
set number
" Turn on syntax highlighting. 
syn on
" Make believe we have 256color terminals
set t_Co=256

" Should really be below in bundle config, but it needs to be here before the
" setup of graphics.
set background=dark

" four space tab
" ( because python!)
" + some indent magic.
set ts=4 sw=4 et

map <C-Tab> :bnext<cr>
map <C-S-Tab> :bprevious<cr>

" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GUI+Terminal differences
if has('gui_running')
	set guifont=Input\ 12
  " This gives us menus, left aligned bars, but not any of the toolbars
  " that come with the standard gVim configuration
  set guioptions=matlcpv
  " Some nicities about being in a gui: we have an X clipboard.
  " I like using C-c and C-v when I'm in insert mode. 
  map <C-c> "+y
  map <C-v> "+gP
  inoremap <C-v> <C-r>*
else
  inoremap <C-v> <C-r>+
endif


" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Bundle configurations

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#whitespace#enabled = 1
let g:airline_theme='PaperColor'
let g:indent_guide_size=1

set mouse=a

colorscheme PaperColor

