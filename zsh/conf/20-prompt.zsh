#
# Prompt initialization
#

autoload -U add-zsh-hook
function powerline_precmd() {
    PS1="$($HOME/.zsh/bin/powerline-shell.py $? --cwd-only --shell zsh 2> /dev/null)"
}

add-zsh-hook precmd powerline_precmd

